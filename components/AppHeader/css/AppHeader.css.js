import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    header: {
        paddingTop: getStatusBarHeight(),
        height: 30 + getStatusBarHeight(),
        backgroundColor: 'green',
        justifyContent: 'center',
        marginBottom: 12
    },
    headerBody: {
        marginBottom: 12,
        marginLeft: 20
    }
});