import React from 'react';
import { Header, Body, Title } from 'native-base';
import Icon from 'react-native-vector-icons/Entypo';

import styles from './css/AppHeader.css'

export default class ResourceView extends React.Component {
    render() {
        return (
            <Header style={styles.header}>
                <Icon name="tree" size={24} color="white" />
                <Body style={styles.headerBody}>
                    <Title>GreenMile I18N Resources</Title>
                </Body>
            </Header>
        );
    }
}