import React from 'react';
import { View } from 'react-native';
import { Card, CardItem, Body, Text, Badge } from 'native-base';
import moment from 'moment';

import styles from './css/ResourceView.css';

export default class ResourceView extends React.Component {

    constructor(props) {
        super(props);
    }

    getDateFormatted(dateStr) {
        if (dateStr === null) {
            return '';
        }
        return moment(new Date(dateStr)).format('LLL');
    }

    render() {
        return (
            <Card>
                <CardItem>
                    <Body>
                        { this.props.resource.updated_at !== null ? 
                            <Badge success>
                                <Text>Updated</Text>
                            </Badge>
                        :   <Badge danger>
                                <Text style={styles.itemNotUpdated}>Updated</Text>
                            </Badge>
                        }

                        <View style={styles.item}>
                            <Text style={styles.labelItem}>Resource: </Text>
                            <Text>{ this.props.resource.resource_id }</Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.labelItem}>Value: </Text>
                            <Text>{ this.props.resource.value }</Text>
                        </View>
                        <View style={styles.item}>
                            <Text style={styles.labelItem}>Updated At: </Text>
                            <Text>{ this.getDateFormatted(this.props.resource.updated_at) }</Text>
                        </View>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}