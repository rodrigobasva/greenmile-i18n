import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    itemNotUpdated: {
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    labelItem: {
        fontWeight: 'bold'
    }
  });