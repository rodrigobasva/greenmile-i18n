import React from 'react';
import Search from '../../components/Search/Search';

import renderer from 'react-test-renderer';

test('Search component renders correctly', () => {
  const tree = renderer.create(<Search allModules={[]}/>).toJSON();
  expect(tree).toMatchSnapshot();
});