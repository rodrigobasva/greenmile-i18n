import React from 'react';
import AppHeader from '../../components/AppHeader/AppHeader';

import renderer from 'react-test-renderer';

test('AppHeader component renders correctly', () => {
  const tree = renderer.create(<AppHeader />).toJSON();
  expect(tree).toMatchSnapshot();
});