import React from 'react';
import renderer from 'react-test-renderer';

import ResourceView from '../../components/ResourceView/ResourceView';

test('ResourceView component renders correctly with a given resource as prop', () => {
  const resource = {
    "resource": {
      "resource_id": "fake_resource",
      "module_id": "fake_module",
      "language_id": "pt"
    }
  }
  const tree = renderer.create(<ResourceView resource={resource}/>).toJSON();
  expect(tree).toMatchSnapshot();
});