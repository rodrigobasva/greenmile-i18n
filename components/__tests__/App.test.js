import React from 'react';
import App from './../../App';

import { shallow } from 'enzyme';

import renderer from 'react-test-renderer';

let resources = [];

beforeAll(() => {
    this.resources = [
        {
            "module_id": "mock_module",
            "language_id": "pt",
            "value": "some nice value",
            "updated_at": "2012-11-18T15:11:23Z"
        },
        {
            "module_id": "mock_module2",
            "language_id": "en",
            "value": "a",
            "updated_at": null
        }
    ];
});

test('App component renders correctly', () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
});

test('Check the function that filters resources by value provided', () => {
    const wrapper = shallow(<App />);
    const instance = wrapper.instance();

    let resourcesFiltered = instance.doFilterByText(this.resources, 'nice');
    expect(resourcesFiltered.length).toBe(1);
});

test('Check the function that filters resources by module provided', () => {
	const wrapper = shallow(<App />);
	const instance = wrapper.instance();

	let resourcesFiltered = instance.doFilterByModule(this.resources, 'mock_module');
	expect(resourcesFiltered.length).toBe(1);
});

test('Check the function that filters resources by language provided', () => {
	const wrapper = shallow(<App />);
	const instance = wrapper.instance();

	let resourcesFiltered = instance.doFilterByLanguage(this.resources, 'en');
	expect(resourcesFiltered.length).toBe(1);
});

test('Check the function that filters resources by updated value provided', () => {
	const wrapper = shallow(<App />);
	const instance = wrapper.instance();

	let resourcesFiltered = instance.doFilterByUpdated(this.resources, 'Yes');
	expect(resourcesFiltered.length).toBe(1);
});