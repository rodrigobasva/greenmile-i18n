import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    searchResourceContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 8
    },
    showButton: {
        justifyContent: 'center',
        marginTop: 12
    },
    expandIconContainer: {
        justifyContent: 'center',
    },
    expandIcon: {
        padding: 10
    }
});