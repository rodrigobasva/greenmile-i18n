import React from 'react';
import { Item, Input, Button, Text, View, Picker } from 'native-base';

import FoundationIcon from 'react-native-vector-icons/Foundation';

import styles from './css/Search.css';

export default class Search extends React.Component {

    languageOptions = [
        {"label": "English", "value": "en"},
        {"label": "Portuguese", "value": "pt"},
        {"label": "German", "value": "de"},
        {"label": "Spanish", "value": "es"},
        {"label": "French", "value": "fr"},
        {"label": "Italian", "value": "it"},
        {"label": "Japanese", "value": "ja"},
        {"label": "Thai", "value": "th"}
    ];

    constructor(props) {
        super(props);

        this.state = {
            allModules: [...this.props.allModules],
            text: '',
            language: null,
            module: null,
            updated: null,
            hided: true
        }
    }

    onLanguageChanged = (value) => {
        this.setState({language: value});
    };

    onModuleChanged = (value) => {
        this.setState({module: value});
    };

    onUpdatedChanged = (value) => {
        this.setState({updated: value});
    };

    render() {
        return this.state.hided ? (
            <View style={styles.expandIconContainer}>
                <Button full light onPress={() => {this.setState({hided: false})}}>
                    <FoundationIcon style={styles.expandIcon} name="arrow-down" size={18}/>
                </Button>
            </View>) : 
            (
            <View>
                <Item rounded style={styles.searchResourceContainer}>
                    <Input
                        onChangeText={text => this.setState({text})}
                        value={this.state.text}
                        placeholder='Search...'
                    />
                </Item>
                <View>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.language}
                            onValueChange={(value) => this.onLanguageChanged(value)}
                        >
                            <Picker.Item label="-- Pick a language --" value={null}/>
                            { this.languageOptions.map((language, index) => 
                                <Picker.Item label={language.label} value={language.value} key={index.toString()}/>) }
                        </Picker>
                    </Item>

                    <Item picker>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.module}
                            onValueChange={(value) => this.onModuleChanged(value)}
                        >
                            <Picker.Item label="-- Pick a module --" value={null}/>
                            { this.state.allModules.map((module, index) => 
                                <Picker.Item label={module} value={module} key={index.toString()}/>) }
                        </Picker>
                    </Item>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            selectedValue={this.state.updated}
                            onValueChange={(value) => this.onUpdatedChanged(value)}
                        >
                            <Picker.Item label="-- Pick updated option --" value={null}/>
                            <Picker.Item label="Yes" value="Yes"/>
                            <Picker.Item label="No" value="No"/>
                        </Picker>
                    </Item>
                    <View>
                        <Button light full onPress={() => {
                            this.setState({hided: true});
                            this.props.onSearch(this.state.text, this.state.language, this.state.module, this.state.updated);
                        }}>
                            <Text>Search</Text>
                        </Button>
                    </View>
                </View>
            </View>);
    }
}