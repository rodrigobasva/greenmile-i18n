import React from 'react';
import { View, FlatList, Image } from 'react-native';
import { Root, Text, Container, Spinner, Toast } from 'native-base';
import { Font } from 'expo';
import _ from 'lodash';

import styles from './App.css';
import AppHeader from './components/AppHeader/AppHeader';
import ResourceView from './components/ResourceView/ResourceView';
import Search from './components/Search/Search';

export default class App extends React.Component {
  
  PAGE_SIZE = 10;

  constructor(props) {
    super(props);

    this.state = {
      fontLoaded: false,
      allResources: {},
      allModules: null,
      resources: {},
      currentPage: 1,
      text: '',
      refresh: false,
      isLoading: true,
      search_text: '',
      search_updated: null,
      search_module: null,
      search_language: null
    }
  }

  componentDidMount() {
    this.fetchResources();
  }

  async componentWillMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  fetchResources() {
    fetch('http://portal.greenmilesoftware.com/get_resources_since')
      .then(response => {
        return response.json();
      })
      .then((data) => {
        let resources = [];
        data.map(item => (
          resources.push({
            resource_id: item.resource.resource_id,
            module_id: item.resource.module_id,
            updated_at: item.resource.updated_at,
            language_id: item.resource.language_id,
            value: item.resource.value
          })));

        this.getModulesInfoFromRequest(resources);
        this.setState({
          isLoading: false,
          allResources: resources,
          resources: resources.slice(0, this.PAGE_SIZE)
        });
        
    })
    .catch(() => {
      this.setState({isLoading: false});
      Toast.show({
        text: "Fail request!",
        type: "danger"
      });
    });
  }

  getModulesInfoFromRequest(resources) {
    let modulesSet = new Set();
    resources.forEach(resource => {
      modulesSet.add(resource.module_id);
    });
    this.setState({
      allModules: modulesSet
    })
  }

  loadMoreResources() {
    if (this.state.search_text === '' && this.state.search_module === null && 
      this.state.search_language === null && this.state.search_updated === null) {
      let moreResources = this.state.allResources.slice(this.state.currentPage * this.PAGE_SIZE,
        (this.state.currentPage + 1) * this.PAGE_SIZE);
  
      this.setState(prevState => ({
        resources: [...prevState.resources, ...moreResources],
        currentPage: prevState.currentPage + 1,
        refresh: !prevState.refresh
      }));
    } else {
      this.setState(prevState => ({
        resources: [...prevState.resources],
        currentPage: 0,
        refresh: !prevState.refresh
      }));
    }
  }

  onSearch = (text, language, module, updated) => {
    let resourcesToShow = [];

    if (text === '' && language == null && module == null && updated == null) {
      resourcesToShow = this.state.allResources;
    } else {
      let filteredResources = this.state.allResources;

      if (text !== '') {
        filteredResources = this.doFilterByText(filteredResources, text);
      }

      if (language != null) {
        filteredResources = this.doFilterByLanguage(filteredResources, language);
      }

      if (module != null) {
        filteredResources = this.doFilterByModule(filteredResources, module);
      }

      if (updated != null) {
        filteredResources = this.doFilterByUpdated(filteredResources, updated);
      }

      resourcesToShow = filteredResources;
    }

    this.setState({
      resources: resourcesToShow,
      search_text: text,
      search_language: language,
      search_updated: updated,
      search_module: module
    });
  };

  doFilterByText(resources, text) {
    return _.filter(resources, resource => _.includes(resource.value.toLowerCase(), text.toLowerCase()));
  }

  doFilterByLanguage(resources, language) {
    return _.filter(resources, resource => {
      return resource.language_id === language;
    });
  }

  doFilterByModule(resources, module) {
    return _.filter(resources, resource => {
      return resource.module_id === module;
    });
  }

  doFilterByUpdated(resources, updated) {
    if (updated === 'Yes') {
      return _.filter(resources, resource => {
        return resource.updated_at !== null;
      });
    } else {
      return _.filter(resources, resource => {
        return resource.updated_at === null;
      });
    }
  }

  renderItem = ({ item }) => (<ResourceView resource={item}/>);

  render() {
    return this.state.fontLoaded ? (
      this.state.isLoading ? (
        <Root>
          <Container>
            <AppHeader/>
            <View style={styles.loadingContainer}>
              <Spinner/>
              <Text>Loading info...</Text>
            </View>
          </Container>
        </Root>
      ) : (
        <Root>
          <Container>
            <AppHeader/>
            <Search allModules={this.state.allModules}
              onSearch={(text, language, module, updated) => this.onSearch(text, language, module, updated)}/>
            {this.state.resources.length > 0 ? (
              <FlatList
                data={this.state.resources}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
                onEndReached={() => this.loadMoreResources()}
                onEndReachedThreshold={0.3}
                extraData={this.state.refresh}
            />) : (
              <Image style={{flex:1, height: undefined, width: undefined}}
                    source={require('./assets/no_results_found.png')}
                    resizeMode="contain"/>
            )}
          </Container>
        </Root>
      )
    ) : null;
  }
}