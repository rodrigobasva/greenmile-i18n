import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    loadingContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }
  });